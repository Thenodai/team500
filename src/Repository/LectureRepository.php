<?php

namespace App\Repository;

use App\Entity\Course;
use App\Entity\Lecture;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Lecture|null find($id, $lockMode = null, $lockVersion = null)
 * @method Lecture|null findOneBy(array $criteria, array $orderBy = null)
 * @method Lecture[]    findAll()
 * @method Lecture[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LectureRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Lecture::class);
    }

    public function findLecturesByTeacher(User $user)
    {
        return $this->createQueryBuilder('l')
            ->where('l.user = :user')
            ->setParameter('user', $user);
    }

    public function findLecturesByStudent(User $user)
    {
        return $this->createQueryBuilder('l')
            ->leftJoin('App\Entity\Course', 'c', 'WITH', 'l.course = c.id')
            ->innerJoin('App\Entity\UserCourse', 'uc', 'WITH', 'c.id = uc.id')
            ->leftJoin('App\Entity\User', 'u', 'WITH', 'uc.user = u.id')
            ->where('u = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();
    }

    public function findLecturesByCourse(Course $course)
    {
        return $this->createQueryBuilder('lectures')
            ->where('lectures.course = :id')
            ->setParameter('id', $course)
            ->getQuery()
            ->getResult();
    }
}
