<?php

namespace App\Repository;

use App\Entity\Course;
use App\Entity\Homework;
use App\Entity\Lecture;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Homework|null find($id, $lockMode = null, $lockVersion = null)
 * @method Homework|null findOneBy(array $criteria, array $orderBy = null)
 * @method Homework[]    findAll()
 * @method Homework[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HomeworkRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Homework::class);
    }

    public function findAllHomeworkByTeacher(User $user)
    {
        return $this->createQueryBuilder('h')
            ->join('h.lecture', 'l', 'WITH', 'l.id = h.lecture')
            ->where('l.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();
    }

    public function findHomeworksByLecture(Lecture $lecture)
    {
        return $this->createQueryBuilder('h')
            ->where('h.lecture = :lecture')
            ->setParameter('lecture', $lecture)
            ->getQuery()
            ->getResult();
    }

    public function findVisibleHomeworksByLecture(Lecture $lecture)
    {
        return $this->createQueryBuilder('h')
            ->where('h.lecture = :lecture AND h.availableFrom < :now')
            ->setParameter('lecture', $lecture)
            ->setParameter('now', (new \DateTime())->format('Y-m-d H:i:s'))
            ->getQuery()
            ->getResult();
    }

    public function findVisibleHomeworksByUser(User $user, int $max = 5)
    {
        return $this->createQueryBuilder('h')
            ->leftJoin('App\Entity\Lecture', 'l', 'WITH', 'h.lecture = l.id')
            ->leftJoin('App\Entity\Course', 'c', 'WITH', 'l.course = c.id')
            ->innerJoin('App\Entity\UserCourse', 'uc', 'WITH', 'c.id = uc.id')
            ->leftJoin('App\Entity\User', 'u', 'WITH', 'uc.user = u.id')
            ->where('u = :user AND h.availableFrom < :now')
            ->setParameter('user', $user)
            ->setParameter('now', (new \DateTime())->format('Y-m-d H:i:s'))
            ->orderBy('h.created_at', 'DESC')
            ->setMaxResults($max)
            ->getQuery()
            ->getResult();
    }
}
