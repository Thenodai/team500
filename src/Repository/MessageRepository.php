<?php

namespace App\Repository;

use App\Entity\Message;
use App\Entity\Thread;
use App\Entity\User;
use App\Service\ThreadInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Message|null find($id, $lockMode = null, $lockVersion = null)
 * @method Message|null findOneBy(array $criteria, array $orderBy = null)
 * @method Message[]    findAll()
 * @method Message[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MessageRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Message::class);
    }

    public function updateOldThreadMessagesAsSeen(ThreadInterface $thread, User $user)
    {
        return $this->createQueryBuilder('m')
            ->update('App\Entity\Message', 'm')
            ->set('m.read', 1)
            ->where('m.thread = :thread AND m.user != :user')
            ->setParameter('thread', $thread)
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findUnreadMessagesByUser(User $user)
    {
        return $this->createQueryBuilder('m')
            ->leftJoin('App\Entity\Thread', 't', 'WITH', 'm.thread = t.id')
            ->innerJoin('t.participants', 'u')
            ->where('u = :user AND m.read = :read AND m.user != :owner')
            ->setParameter('owner', $user)
            ->setParameter('user', $user)
            ->setParameter('read', 0)
            ->getQuery()
            ->getResult()
        ;
    }
}
