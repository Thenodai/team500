<?php

namespace App\Repository;

use App\Entity\SolvedHomework;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method SolvedHomework|null find($id, $lockMode = null, $lockVersion = null)
 * @method SolvedHomework|null findOneBy(array $criteria, array $orderBy = null)
 * @method SolvedHomework[]    findAll()
 * @method SolvedHomework[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SolvedHomeworkRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, SolvedHomework::class);
    }

    public function findSolvedHomeworkByHomework($homework, $user)
    {
        return $this->createQueryBuilder('sh')
            ->where('sh.homework = :homework AND sh.user = :user')
            ->setParameter('homework', $homework)
            ->setParameter('user', $user)
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();
    }

    public function findSolvedHomeworksByHomework($homework)
    {
        return $this->createQueryBuilder('sh')
            ->addSelect('u')
            ->leftJoin('sh.user', 'u')
            ->where('sh.homework = :homework')
            ->setParameter('homework', $homework)
            ->getQuery()
            ->getResult();
    }

    public function setMark(SolvedHomework $solvedHomework, $mark)
    {
        $this->createQueryBuilder('sh')
            ->update()
            ->set('sh.mark', $mark)
            ->where('sh.id = :id')
            ->setParameter('id', $solvedHomework)
            ->getQuery()
            ->execute();
    }

    public function findSolvedHomeworksWithMarks(User $user, int $max = 5)
    {
        return $this->createQueryBuilder('sh')
            ->where('sh.user = :user')
            ->andWhere('sh.mark IS NOT NULL')
            ->setParameter('user', $user)
            ->setMaxResults($max)
            ->getQuery()
            ->getResult();
    }

    public function findUnmarkedHomework(User $user)
    {
        return $this->createQueryBuilder('sh')
            ->leftJoin('sh.homework', 'h')
            ->leftJoin('h.lecture', 'l')
            ->where('l.user = :user AND sh.mark IS NULL')
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult()
        ;
    }
}
