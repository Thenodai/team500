<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180526104852 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE course (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, title VARCHAR(100) NOT NULL, description VARCHAR(250) NOT NULL, INDEX IDX_169E6FB9A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE homework (id INT AUTO_INCREMENT NOT NULL, lecture_id INT NOT NULL, user_id INT NOT NULL, title VARCHAR(100) NOT NULL, description VARCHAR(250) NOT NULL, type VARCHAR(15) NOT NULL, available_from DATETIME DEFAULT NULL, created_at DATETIME NOT NULL, INDEX IDX_8C600B4E35E32FCD (lecture_id), INDEX IDX_8C600B4EA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE lecture (id INT AUTO_INCREMENT NOT NULL, course_id INT NOT NULL, user_id INT NOT NULL, title VARCHAR(100) NOT NULL, description VARCHAR(250) NOT NULL, INDEX IDX_C1677948591CC992 (course_id), INDEX IDX_C1677948A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE message (id INT AUTO_INCREMENT NOT NULL, thread_id INT DEFAULT NULL, user_id INT DEFAULT NULL, body VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT NULL, seen TINYINT(1) NOT NULL, INDEX IDX_B6BD307FE2904019 (thread_id), INDEX IDX_B6BD307FA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE solved_homework (id INT AUTO_INCREMENT NOT NULL, homework_id INT NOT NULL, user_id INT NOT NULL, title VARCHAR(250) DEFAULT NULL, link VARCHAR(250) DEFAULT NULL, file VARCHAR(250) DEFAULT NULL, mark INT DEFAULT NULL, text LONGTEXT DEFAULT NULL, INDEX IDX_F8A30BACB203DDE5 (homework_id), INDEX IDX_F8A30BACA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE thread (id INT AUTO_INCREMENT NOT NULL, subject VARCHAR(64) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_user (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) NOT NULL, email VARCHAR(64) NOT NULL, password VARCHAR(250) NOT NULL, role VARCHAR(50) NOT NULL, UNIQUE INDEX UNIQ_88BDF3E9E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users_threads (user_id INT NOT NULL, thread_id INT NOT NULL, INDEX IDX_9F8E4917A76ED395 (user_id), INDEX IDX_9F8E4917E2904019 (thread_id), PRIMARY KEY(user_id, thread_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users_courses (id INT AUTO_INCREMENT NOT NULL, course_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_59A52E86591CC992 (course_id), INDEX IDX_59A52E86A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE course ADD CONSTRAINT FK_169E6FB9A76ED395 FOREIGN KEY (user_id) REFERENCES app_user (id)');
        $this->addSql('ALTER TABLE homework ADD CONSTRAINT FK_8C600B4E35E32FCD FOREIGN KEY (lecture_id) REFERENCES lecture (id)');
        $this->addSql('ALTER TABLE homework ADD CONSTRAINT FK_8C600B4EA76ED395 FOREIGN KEY (user_id) REFERENCES app_user (id)');
        $this->addSql('ALTER TABLE lecture ADD CONSTRAINT FK_C1677948591CC992 FOREIGN KEY (course_id) REFERENCES course (id)');
        $this->addSql('ALTER TABLE lecture ADD CONSTRAINT FK_C1677948A76ED395 FOREIGN KEY (user_id) REFERENCES app_user (id)');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307FE2904019 FOREIGN KEY (thread_id) REFERENCES thread (id)');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307FA76ED395 FOREIGN KEY (user_id) REFERENCES app_user (id)');
        $this->addSql('ALTER TABLE solved_homework ADD CONSTRAINT FK_F8A30BACB203DDE5 FOREIGN KEY (homework_id) REFERENCES homework (id)');
        $this->addSql('ALTER TABLE solved_homework ADD CONSTRAINT FK_F8A30BACA76ED395 FOREIGN KEY (user_id) REFERENCES app_user (id)');
        $this->addSql('ALTER TABLE users_threads ADD CONSTRAINT FK_9F8E4917A76ED395 FOREIGN KEY (user_id) REFERENCES app_user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE users_threads ADD CONSTRAINT FK_9F8E4917E2904019 FOREIGN KEY (thread_id) REFERENCES thread (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE users_courses ADD CONSTRAINT FK_59A52E86591CC992 FOREIGN KEY (course_id) REFERENCES course (id)');
        $this->addSql('ALTER TABLE users_courses ADD CONSTRAINT FK_59A52E86A76ED395 FOREIGN KEY (user_id) REFERENCES app_user (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE lecture DROP FOREIGN KEY FK_C1677948591CC992');
        $this->addSql('ALTER TABLE users_courses DROP FOREIGN KEY FK_59A52E86591CC992');
        $this->addSql('ALTER TABLE solved_homework DROP FOREIGN KEY FK_F8A30BACB203DDE5');
        $this->addSql('ALTER TABLE homework DROP FOREIGN KEY FK_8C600B4E35E32FCD');
        $this->addSql('ALTER TABLE message DROP FOREIGN KEY FK_B6BD307FE2904019');
        $this->addSql('ALTER TABLE users_threads DROP FOREIGN KEY FK_9F8E4917E2904019');
        $this->addSql('ALTER TABLE course DROP FOREIGN KEY FK_169E6FB9A76ED395');
        $this->addSql('ALTER TABLE homework DROP FOREIGN KEY FK_8C600B4EA76ED395');
        $this->addSql('ALTER TABLE lecture DROP FOREIGN KEY FK_C1677948A76ED395');
        $this->addSql('ALTER TABLE message DROP FOREIGN KEY FK_B6BD307FA76ED395');
        $this->addSql('ALTER TABLE solved_homework DROP FOREIGN KEY FK_F8A30BACA76ED395');
        $this->addSql('ALTER TABLE users_threads DROP FOREIGN KEY FK_9F8E4917A76ED395');
        $this->addSql('ALTER TABLE users_courses DROP FOREIGN KEY FK_59A52E86A76ED395');
        $this->addSql('DROP TABLE course');
        $this->addSql('DROP TABLE homework');
        $this->addSql('DROP TABLE lecture');
        $this->addSql('DROP TABLE message');
        $this->addSql('DROP TABLE solved_homework');
        $this->addSql('DROP TABLE thread');
        $this->addSql('DROP TABLE app_user');
        $this->addSql('DROP TABLE users_threads');
        $this->addSql('DROP TABLE users_courses');
    }
}
