<?php

namespace App\DataFixtures;

use App\Entity\Course;
use App\Entity\Homework;
use App\Entity\Lecture;
use App\Entity\User;
use App\Entity\UserCourse;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Validator\Constraints\DateTime;

class AdminFixtures extends Fixture
{



    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setName('Admin');
        $user->setRole('ROLE_ADMIN');
        $user->setEmail('admin@admin.com');
        $user->setPassword('$2y$10$Urxx..fwDVimsDx3O8RdIOKtpp19xmoswHSdwH11oFYbcVBXW0IQi'); //password
        $manager->persist($user);

        for ($i = 0; $i < 30; $i ++){
            $user = new User();
            $user->setName("Student$i");
            $user->setRole('ROLE_STUDENT');
            $user->setEmail("student$i@uni.com");
            $user->setPassword('$2y$10$Urxx..fwDVimsDx3O8RdIOKtpp19xmoswHSdwH11oFYbcVBXW0IQi'); //password
            $manager->persist($user);
        }

        for ($i = 0; $i < 10; $i ++){
            $user = new User();
            $user->setName("Teacher$i");
            $user->setRole('ROLE_TEACHER');
            $user->setEmail("teacher$i@uni.com");
            $user->setPassword('$2y$10$Urxx..fwDVimsDx3O8RdIOKtpp19xmoswHSdwH11oFYbcVBXW0IQi'); //password
            $manager->persist($user);
            $manager->flush();
        }

        $teachers = $manager->getRepository(User::class)->findBy(['role' => 'ROLE_TEACHER']);

        for ($i = 0; $i < 8; $i++)
        {
            $course = new Course();
            $course->setTitle("Course Nr.$i");
            $course->setDescription("This is Course Nr. $i. You will learn a lot of useful things this semester. Enjoy.");
            $course->setUser($teachers[$i]);
            $manager->persist($course);
            $manager->flush();
        }

        $students = $manager->getRepository(User::class)->findBy(['role' => 'ROLE_STUDENT']);
        $courses = $manager->getRepository(Course::class)->findAll();

        for ($i = 0; $i <100; $i++)
        {
            $student = $students[rand(1,20)];
            $course = $courses[rand(0,7)];
            $userCourse = new UserCourse;
            $userCourse->setUser($student);
            $userCourse->setCourse($course);
            $manager->persist($userCourse);
            $manager->flush();
        }

        for ($i = 0;$i < 60;$i++){
            $course = $courses[rand(0,7)];
            $lecture = new Lecture();
            $lecture->setCourse($course);
            $lecture->setTitle("Lecture Nr. $i");
            $lecture->setDescription("Today we will learn the number of $i");
            $lecture->setUser($lecture->getCourse()->getUser());
            $manager->persist($lecture);
            $manager->flush();
        }
        
        $manager->flush();
    }
}
