<?php

namespace App\Event;

use App\Entity\Message;
use App\Entity\Thread;
use App\Entity\User;
use Symfony\Component\EventDispatcher\Event;

class MessageReplyEvent extends Event
{
    /**
     * @var Message
     */
    private $message;

    /**
     * @var Thread
     */
    protected $thread;

    /**
     * @var User
     */
    protected $user;

    public function __construct(Message $message, Thread $thread, User $user)
    {
        $this->message = $message;
        $this->thread = $thread;
        $this->user = $user;
    }

    /**
     * @return Message
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return Thread
     */
    public function getThread()
    {
        return $this->thread;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }
}
