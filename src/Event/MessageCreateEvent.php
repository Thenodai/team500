<?php

namespace App\Event;

use App\Entity\Message;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class MessageCreateEvent extends Event
{
    /**
     * @var FormInterface
     */
    private $form;
    /**
     * @var Message
     */
    private $message;

    /**
     * @var UserInterface
     */
    protected $user;

    public function __construct(FormInterface $form, Message $message, UserInterface $user)
    {
        $this->form = $form;
        $this->message = $message;
        $this->user = $user;
    }

    /**
     * @return Message
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return FormInterface
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * @return UserInterface
     */
    public function getUser()
    {
        return $this->user;
    }
}
