<?php

namespace App\Event;

use App\Entity\Thread;
use App\Entity\User;
use Symfony\Component\EventDispatcher\Event;

class MessageSeenEvent extends Event
{
    /**
     * @var Thread
     */
    protected $thread;

    /**
     * @var User
     */
    protected $user;

    public function __construct(Thread $thread, User $user)
    {
        $this->thread = $thread;
        $this->user = $user;
    }

    /**
     * @return Thread
     */
    public function getThread()
    {
        return $this->thread;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }
}
