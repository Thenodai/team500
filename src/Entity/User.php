<?php

namespace App\Entity;

use App\Service\MessageInterface;
use App\Service\ThreadInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="app_user")
 * @UniqueEntity("email")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(max="100")
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(max="64")
     * @Assert\Email()
     * @ORM\Column(type="string", length=64, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=250)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $role;

    /**
     * @ORM\OneToMany(targetEntity="Course", mappedBy="user")
     */
    private $courses;

    /**
     * @ORM\OneToMany(targetEntity="Lecture", mappedBy="user")
     */
    private $lectures;

    /**
     * @ORM\OneToMany(targetEntity="Homework", mappedBy="user")
     */
    private $homeworks;

    /**
     * @ORM\OneToMany(targetEntity="SolvedHomework", mappedBy="user")
     */
    private $solvedHomeworks;

    /**
     * @ORM\OneToMany(targetEntity="UserCourse", mappedBy="user")
     */
    private $userCourse;

    /**
     * @var ThreadInterface[]|ArrayCollection
     * @ORM\ManyToMany(targetEntity="App\Entity\Thread", inversedBy="participants", cascade={"persist"})
     * @ORM\JoinTable(name="users_threads")
     */
    private $threads;

    /**
     * @var MessageInterface
     * @ORM\OneToMany(targetEntity="App\Entity\Message", mappedBy="user")
     */
    private $messages;

    public function __construct()
    {
        $this->courses = new ArrayCollection();
        $this->lectures = new ArrayCollection();
        $this->homeworks = new ArrayCollection();
        $this->solvedHomeworks = new ArrayCollection();
        $this->userCourse = new ArrayCollection();
        $this->threads = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUsername()
    {
        return $this->email;
    }

    public function setEmail(string $email): User
    {
        $this->email = $email;
        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }


    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getRole(): ?string
    {
        return $this->role;
    }

    public function setRole(string $role): self
    {
        $this->role = $role;

        return $this;
    }

    /**
     * @return Collection|Course[]
     */
    public function getCourses(): Collection
    {
        return $this->courses;
    }

    public function addCourse(Course $course): self
    {
        if (!$this->courses->contains($course)) {
            $this->courses[] = $course;
            $course->setUser($this);
        }

        return $this;
    }

    public function removeCourse(Course $course): self
    {
        if ($this->courses->contains($course)) {
            $this->courses->removeElement($course);
            // set the owning side to null (unless already changed)
            if ($course->getUser() === $this) {
                $course->setUser(null);
            }
        }

        return $this;
    }

    public function getLectures()
    {
        return $this->lectures;
    }

    public function addLecture(Lecture $article): self
    {
        if (!$this->lectures->contains($article)) {
            $this->lectures[] = $article;
            $article->setUser($this);
        }

        return $this;
    }

    public function removeLecture(Lecture $article): self
    {
        if ($this->lectures->contains($article)) {
            $this->lectures->removeElement($article);
            // set the owning side to null (unless already changed)
            if ($article->getUser() === $this) {
                $article->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Homework[]
     */
    public function getHomeworks(): Collection
    {
        return $this->homeworks;
    }

    public function addHomework(Homework $homework): self
    {
        if (!$this->homeworks->contains($homework)) {
            $this->homeworks[] = $homework;
            $homework->setUser($this);
        }

        return $this;
    }

    public function removeHomework(Homework $homework): self
    {
        if ($this->homeworks->contains($homework)) {
            $this->homeworks->removeElement($homework);
            // set the owning side to null (unless already changed)
            if ($homework->getUser() === $this) {
                $homework->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|SolvedHomework[]
     */
    public function getSolvedHomeworks(): Collection
    {
        return $this->solvedHomeworks;
    }

    public function addSolvedHomework(SolvedHomework $solvedHomework): self
    {
        if (!$this->solvedHomeworks->contains($solvedHomework)) {
            $this->solvedHomeworks[] = $solvedHomework;
            $solvedHomework->setUser($this);
        }

        return $this;
    }

    public function removeSolvedHomework(SolvedHomework $solvedHomework): self
    {
        if ($this->solvedHomeworks->contains($solvedHomework)) {
            $this->solvedHomeworks->removeElement($solvedHomework);
            // set the owning side to null (unless already changed)
            if ($solvedHomework->getUser() === $this) {
                $solvedHomework->setUser(null);
            }
        }

        return $this;
    }

    public function getRoles()
    {
        return [$this->role];
    }

    public function getSalt()
    {
        return null;
    }

    public function eraseCredentials()
    {
        return null;
    }

    /**
     * @return Collection|UserCourse[]
     */
    public function getUserCourse(): Collection
    {
        return $this->userCourse;
    }

    public function addUserCourse(UserCourse $userCourse): self
    {
        if (!$this->userCourse->contains($userCourse)) {
            $this->userCourse[] = $userCourse;
            $userCourse->setUser($this);
        }

        return $this;
    }

    public function removeUserCourse(UserCourse $userCourse): self
    {
        if ($this->userCourse->contains($userCourse)) {
            $this->userCourse->removeElement($userCourse);
            // set the owning side to null (unless already changed)
            if ($userCourse->getUser() === $this) {
                $userCourse->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @param ThreadInterface $thread
     * @return $this
     */
    public function addThread(ThreadInterface $thread)
    {
        if ($this->threads->contains($thread)) {
            return;
        }
        $this->threads[] = $thread;
        $thread->addParticipant($this);
        return $this;
    }

    /**
     * @return ThreadInterface[]|ArrayCollection
     */
    public function getThreads()
    {
        return $this->threads;
    }

    /**
     * @param ThreadInterface $thread
     */
    public function removeThread(ThreadInterface $thread)
    {
        if (!$this->threads->contains($thread)) {
            return;
        }
        $this->threads->removeElement($thread);
        $thread->removeParticipant($this);
    }

    /**
     * @return MessageInterface
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param MessageInterface $message
     * @return User
     */
    public function setMessage(MessageInterface $message)
    {
        $this->messages = $message;
        return $this;
    }

}
