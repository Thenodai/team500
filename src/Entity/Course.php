<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CourseRepository")
 */
class Course
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(min="3", max="100")
     * @ORM\Column(type="string", length=100)
     */
    private $title;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(min="10", max="250")
     * @ORM\Column(type="string", length=250)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="courses")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="Lecture", mappedBy="course")
     */
    private $lectures;

    /**
     * @ORM\OneToMany(
     *     targetEntity="UserCourse",
     *     mappedBy="course",
     *     fetch="EXTRA_LAZY",
     *     orphanRemoval=true,
     *     cascade={"persist"}
     *     )
     * @Assert\Valid()
     */
    private $userCourse;

    public function __construct()
    {
        $this->lectures = new ArrayCollection();
        $this->userCourse = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Lecture[]
     */
    public function getLectures(): Collection
    {
        return $this->lectures;
    }

    public function addLecture(Lecture $lecture): self
    {
        if (!$this->lectures->contains($lecture)) {
            $this->lectures[] = $lecture;
            $lecture->setLecture($this);
        }

        return $this;
    }

    public function removeLecture(Lecture $lecture): self
    {
        if ($this->lectures->contains($lecture)) {
            $this->lectures->removeElement($lecture);
            // set the owning side to null (unless already changed)
            if ($lecture->getLecture() === $this) {
                $lecture->setLecture(null);
            }
        }

        return $this;
    }

    public function addUserCourse(UserCourse $userCourse)
    {
        if ($this->userCourse->contains($userCourse)) {
            return;
        }

        $this->userCourse[] = $userCourse;

        $userCourse->setCourse($this);
    }

    public function removeUserCourse(UserCourse $userCourse)
    {
        if (!$this->userCourse->contains($userCourse)) {
            return;
        }

        $this->userCourse->removeElement($userCourse);

        $userCourse->setCourse(null);
    }

    /**
     * @return ArrayCollection|UserCourse[]
     */
    public function getUserCourse(): Collection
    {
        return $this->userCourse;
    }

}
