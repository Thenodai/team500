<?php

namespace App\Entity;

use App\Service\MessageInterface;
use App\Service\ThreadInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ThreadRepository")
 */
class Thread implements ThreadInterface
{
    /**
     * @var integer
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(max="64")
     * @ORM\Column(name="subject", type="string", length=64)
     */
    private $subject;

    /**
     * @var MessageInterface
     * @ORM\OneToMany(targetEntity="App\Entity\Message", mappedBy="thread", cascade={"remove"})
     */
    private $messages;

    /**
     * @var UserInterface[]|ArrayCollection
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="threads", cascade={"persist"})
     */
    private $participants;

    public function __construct()
    {
        $this->participants = new ArrayCollection();
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $subject
     * @return $this
     */
    public function setSubject(string $subject)
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @return MessageInterface[]
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param MessageInterface $message
     * @return $this
     */
    public function setMessages(MessageInterface $message)
    {
        $this->messages = $message;
        return $this;
    }

    /**
     * @param UserInterface $user
     * @return $this
     */
    public function addParticipant(UserInterface $user)
    {
        if ($this->participants->contains($user)) {
            return;
        }
        $this->participants[] = $user;
        $user->addThread($this);
        return $this;
    }

    /**
     * @return UserInterface[]|ArrayCollection
     */
    public function getParticipants()
    {
        return $this->participants;
    }

    /**
     * @param UserInterface $user
     */
    public function removeParticipant(UserInterface $user)
    {
        if (!$this->participants->contains($user)) {
            return;
        }
        $this->participants->removeElement($user);
        $user->removeThread($this);
    }
}
