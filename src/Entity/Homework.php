<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HomeworkRepository")
 */
class Homework
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(min="3", max="100")
     * @ORM\Column(type="string", length=100)
     */
    private $title;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(min="3", max="250")
     * @ORM\Column(type="string", length=250)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $type;

    /**
     * @var \DateTimeInterface
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $availableFrom;

    /**
     * @ORM\ManyToOne(targetEntity="Lecture", inversedBy="homeworks", fetch="EAGER")
     * @ORM\JoinColumn(nullable=false)
     */
    private $lecture;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="homeworks")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="SolvedHomework", mappedBy="homework")
     */
    private $solvedHomeworks;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    public function __construct()
    {
        $this->solvedHomeworks = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getAvailableFrom()
    {
        return $this->availableFrom;
    }

    public function setAvailableFrom(\DateTime $availableFrom)
    {
        $this->availableFrom = $availableFrom;

        return $this;
    }

    public function getLecture(): ?Lecture
    {
        return $this->lecture;
    }

    public function setLecture(?Lecture $lecture): self
    {
        $this->lecture = $lecture;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|SolvedHomework[]
     */
    public function getSolvedHomeworks(): Collection
    {
        return $this->solvedHomeworks;
    }

    public function addSolvedHomework(SolvedHomework $solvedHomework): self
    {
        if (!$this->solvedHomeworks->contains($solvedHomework)) {
            $this->solvedHomeworks[] = $solvedHomework;
            $solvedHomework->setHomework($this);
        }

        return $this;
    }

    public function removeSolvedHomework(SolvedHomework $solvedHomework): self
    {
        if ($this->solvedHomeworks->contains($solvedHomework)) {
            $this->solvedHomeworks->removeElement($solvedHomework);
            // set the owning side to null (unless already changed)
            if ($solvedHomework->getHomework() === $this) {
                $solvedHomework->setHomework(null);
            }
        }

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }
}
