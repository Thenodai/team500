<?php

namespace App\Entity;

use App\Service\MessageInterface;
use App\Service\ThreadInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MessageRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Message implements MessageInterface
{
    /**
     * @var integer
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(max="255")
     * @ORM\Column(name="body", type="string", length=255)
     */
    private $body;

    /**
     * @var \DateTimeInterface
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var boolean
     * @ORM\Column(name="seen", type="boolean", nullable=false)
     */
    private $read;

    /**
     * @var ThreadInterface
     * @ORM\ManyToOne(targetEntity="App\Entity\Thread", inversedBy="messages", cascade={"persist"})
     * @ORM\JoinColumn(name="thread_id", referencedColumnName="id")
     */
    private $thread;

    /**
     * @var UserInterface
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="messages", fetch="EAGER", cascade={"persist"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $body
     * @return $this
     */
    public function setBody(string $body)
    {
        $this->body = $body;
        return $this;
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @return $this
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();
        return $this;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return ThreadInterface
     */
    public function getThread()
    {
        return $this->thread;
    }

    /**
     * @param ThreadInterface $thread
     * @return $this
     */
    public function setThread(ThreadInterface $thread)
    {
        $this->thread = $thread;
        return $this;
    }

    /**
     * @return UserInterface
     */
    public function getOwner()
    {
        return $this->user;
    }

    /**
     * @param UserInterface $user
     * @return $this
     */
    public function setOwner(UserInterface $user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return bool
     */
    public function isRead()
    {
        return $this->read;
    }

    /**
     * @param bool $read
     * @return $this
     */
    public function setRead(bool $read)
    {
        $this->read = $read;
        return $this;
    }
}
