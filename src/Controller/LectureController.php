<?php

namespace App\Controller;

use App\Entity\Lecture;
use App\Entity\User;
use App\Form\LectureType;
use App\Repository\LectureRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/lecture")
 */
class LectureController extends Controller
{

    public function index(LectureRepository $lectureRepository): Response
    {
        return $this->render('lecture/index.html.twig', ['lectures' => $lectureRepository->findAll()]);
    }

    public function new(Request $request): Response
    {
        $lecture = new Lecture();
        $this->denyAccessUnlessGranted('create', $lecture);

        if ($this->getUser() instanceof User) {
            $lecture->setUser($this->getUser());
        }

        $form = $this->createForm(LectureType::class, $lecture, [
            'user' => $this->getUser()
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($lecture);
            $em->flush();

            return $this->redirectToRoute('lecture_index');
        }

        return $this->render('lecture/new.html.twig', [
            'lecture' => $lecture,
            'form' => $form->createView(),
        ]);
    }

    public function show(Lecture $lecture): Response
    {
        $this->denyAccessUnlessGranted('show', $lecture);

        return $this->render('lecture/show.html.twig', ['lecture' => $lecture]);
    }

    public function edit(Request $request, Lecture $lecture): Response
    {
        $this->denyAccessUnlessGranted('edit', $lecture);
        //todo:: must find better implementation. Teacheris negali priskirti paskaitu prie kursu, kuriems jis nera owneris. zodziu patvarkytas selectas, kad rodytu kursus, kam jis yra owneris.
        $form = $this->createForm(LectureType::class, $lecture, [
            'user' => $this->getUser()
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('lecture_index');
        }

        return $this->render('lecture/edit.html.twig', [
            'lecture' => $lecture,
            'form' => $form->createView(),
        ]);
    }

    public function delete(Request $request, Lecture $lecture): Response
    {
        $this->denyAccessUnlessGranted('delete', $lecture);
        if ($this->isCsrfTokenValid('delete' . $lecture->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($lecture);
            $em->flush();
        }

        return $this->redirectToRoute('lecture_index');
    }
}
