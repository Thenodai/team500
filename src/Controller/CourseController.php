<?php

namespace App\Controller;

use App\Entity\Course;
use App\Entity\User;
use App\Entity\UserCourse;
use App\Form\CourseType;
use App\Form\UserCourseType;
use App\Service\CourseManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CourseController extends Controller
{
    /**
     * @param CourseManager $courseManager
     * @return Response
     */
    public function index(CourseManager $courseManager)
    {
        $courses = $courseManager->getCoursesByUser($this->getUser());

        return $this->render('course/index.html.twig', ['courses' => $courses]);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function new(Request $request)
    {
        $course = new Course();
        $this->denyAccessUnlessGranted('create', $course);
        $form = $this->createForm(CourseType::class, $course);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $course = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $course->setUser($this->getUser());
            $em->persist($course);
            $em->flush();

            return $this->redirectToRoute('course_index');
        }

        return $this->render('course/new.html.twig', [
//            'course' => $course,
            'form' => $form->createView(),
        ]);
    }


    public function show(Course $course)
    {
        $this->denyAccessUnlessGranted('show', $course);
        return $this->render('course/show.html.twig', ['course' => $course]);
    }

    /**
     * @param Request $request
     * @param Course $course
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function edit(Request $request, Course $course)
    {
        $this->denyAccessUnlessGranted('edit', $course);
        $form = $this->createForm(CourseType::class, $course);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('course_edit', ['id' => $course->getId()]);
        }

        return $this->render('course/edit.html.twig', [
            'course' => $course,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Request $request
     * @param Course $course
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete(Request $request, Course $course)
    {
        $this->denyAccessUnlessGranted('delete', $course);
        if ($this->isCsrfTokenValid('delete'.$course->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($course);
            $em->flush();
        }

        return $this->redirectToRoute('course_index');
    }


    public function removeUserCourse($courseId, $userId)
    {
        $em = $this->getDoctrine()->getManager();

        $userCourse = $em->getRepository('App:UserCourse')
            ->findOneBy([
                'course' => $courseId,
                'user' => $userId
            ]);

        $em->remove($userCourse);
        $em->flush();

        return $this->redirectToRoute('course_edit',['id' => $courseId]);
    }
}


