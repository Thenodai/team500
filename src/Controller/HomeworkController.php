<?php

namespace App\Controller;

use App\Entity\Homework;
use App\Entity\SolvedHomework;
use App\Form\HomeworkType;
use App\Form\MarkType;
use App\Provider\HomeworkProvider;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class HomeworkController extends Controller
{
    /**
     * @param HomeworkProvider $homeworkProvider
     * @return Response
     */
    public function index(HomeworkProvider $homeworkProvider): Response
    {
        $homework = $homeworkProvider->getHomeworksByTeacher($this->getUser()->getLectures()->getOwner());
        return $this->render('homework/index.html.twig', ['homeworks' => $homework]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function new(Request $request)
    {
        $homework = new Homework();
        $this->denyAccessUnlessGranted('create', $homework);
        $form = $this->createForm(HomeworkType::class, $homework, [
            'user' => $this->getUser(),
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $homework->setCreatedAt(new \DateTime(date("Y-m-d H:i:s")));
            $homework->setUser($this->getUser());
            $em->persist($homework);
            $em->flush();

            return $this->redirectToRoute('homework_index');
        }

        return $this->render('homework/new.html.twig', [
            'homework' => $homework,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Homework $homework
     * @return Response
     */
    public function show(Homework $homework): Response
    {
        $this->denyAccessUnlessGranted('show', $homework);

        $solvedHomeworks = $this->getDoctrine()->getRepository(SolvedHomework::class)->findSolvedHomeworksByHomework($homework);

        return $this->render('homework/show.html.twig', ['homework' => $homework, 'solvedHomeworks' => $solvedHomeworks]);
    }

    /**
     * @param SolvedHomework $solvedHomework
     * @return Response
     */
    public function solvedHomework(SolvedHomework $solvedHomework, Request $request): Response
    {
        $form = $this->createForm(MarkType::class, $solvedHomework);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($solvedHomework);
            $em->flush();
        }

        return $this->render('homework/showsolved.html.twig', [
            'solvedhomework' => $solvedHomework,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Request $request
     * @param Homework $homework
     * @return Response
     */
    public function edit(Request $request, Homework $homework): Response
    {
        $this->denyAccessUnlessGranted('edit', $homework);
        $form = $this->createForm(HomeworkType::class, $homework, [
            'user' => $this->getUser(),
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('homework_index');
        }

        return $this->render('homework/edit.html.twig', [
            'homework' => $homework,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Request $request
     * @param Homework $homework
     * @return Response
     */
    public function delete(Request $request, Homework $homework): Response
    {
        $this->denyAccessUnlessGranted('delete', $homework);
        if ($this->isCsrfTokenValid('delete'.$homework->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($homework);
            $em->flush();
        }

        return $this->redirectToRoute('homework_index');
    }
}
