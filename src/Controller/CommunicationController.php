<?php

namespace App\Controller;

use App\Entity\Message;
use App\Entity\Thread;
use App\Event\MessageCreateEvent;
use App\Event\MessageReplyEvent;
use App\Event\MessageSeenEvent;
use App\Form\NewThreadMessageFormType;
use App\Form\ReplyMessageType;
use App\Provider\CommunicationProvider;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class CommunicationController extends Controller
{
    public function showInbox(CommunicationProvider $communicationManager)
    {
        $threads = $communicationManager->getThreadsByUser($this->getUser());

        return $this->render('communication/index.html.twig', ['threads' => $threads]);
    }

    public function showThread(Thread $thread, Request $request)
    {
        $this->denyAccessUnlessGranted('show_communication', $thread);

        $message = new Message();
        $form = $this->createForm(ReplyMessageType::class, $message);

        $messageSeenEvent = new MessageSeenEvent($thread, $this->getUser());
        $this->get('event_dispatcher')->dispatch('communication.shown', $messageSeenEvent);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $messageReplyEvent = new MessageReplyEvent($message, $thread, $this->getUser());
            $this->get('event_dispatcher')->dispatch('communication.reply', $messageReplyEvent);

            $em->persist($message);
            $em->flush();
            return $this->redirectToRoute('show_communication', ['id' => $thread->getId()]);
        }

        return $this->render('communication/show_thread.html.twig', [
            'thread' => $thread,
            'form' => $form->createView(),
        ]);
    }

    public function newMessage(Request $request)
    {
        $message = new Message();
        $form = $this->createForm(NewThreadMessageFormType::class, $message, [
            'user' => $this->getUser(),
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $messageCreateEvent = new MessageCreateEvent($form, $message, $this->getUser());
            $this->get('event_dispatcher')->dispatch('communication.create', $messageCreateEvent);

            $em->persist($message);
            $em->flush();

            return $this->redirectToRoute('index_communication');
        }

        return $this->render('communication/new_thread.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    public function deleteThread(Request $request, Thread $thread)
    {
        $this->denyAccessUnlessGranted('delete_thread', $thread);
        if ($this->isCsrfTokenValid('delete' . $thread->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($thread);
            $em->flush();
        }

        return $this->redirectToRoute('index_communication');
    }

    public function deleteMessage(Request $request, Message $message)
    {
        $this->denyAccessUnlessGranted('delete_message', $message);
        if ($this->isCsrfTokenValid('delete' . $message->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($message);
            $em->flush();
        }

        return $this->redirectToRoute('index_communication');
    }
}
