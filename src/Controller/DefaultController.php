<?php

namespace App\Controller;

use App\Entity\Lecture;
use App\Entity\Message;
use App\Entity\SolvedHomework;
use App\Provider\CommunicationProvider;
use App\Provider\HomeworkProvider;
use App\Repository\LectureRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class DefaultController extends Controller
{
    public function index(AuthorizationCheckerInterface $authorizationChecker)
    {
        if ($authorizationChecker->isGranted('ROLE_TEACHER')) {
            return $this->redirectToRoute('admin_dashboard');
        } elseif ($authorizationChecker->isGranted('ROLE_STUDENT')) {
            return $this->redirectToRoute('student_dashboard');
        }

        return $this->render('default/index.html.twig');
    }

    public function admin(CommunicationProvider $communicationManager, HomeworkProvider $homeworkProvider, LectureRepository $lectureRepository)
    {
        //todo: jauciu sita reik iskelt i kita klase. t.p template reikia padaryt kad rodytu, ka reikia, sitie sufetchina.
        //aisku cia tas lectures tai belekaip sudinai atrodo, bet oh well.
        $user = $this->getUser()->getRole();
        if ($user === 'ROLE_ADMIN'){
            $lectures = $this->getDoctrine()->getRepository(Lecture::class)->findAll();
            $messages = $this->getDoctrine()->getRepository(Message::class)->findAll();
            $homeworks = $this->getDoctrine()->getRepository(SolvedHomework::class)->findAll();
        } else{
            $lectures = $lectureRepository->findLecturesByTeacher($this->getUser())->setMaxResults(5)->orderBy('l.id', 'DESC')->getQuery()->getResult();
            $messages = $communicationManager->getUnreadMessagesByUser($this->getUser());
            $homeworks = $homeworkProvider->getUnmarkedHomework($this->getUser());
        }

        return $this->render('default/admin.html.twig', [
            'lectures' => $lectures,
            'messages' => $messages,
            'homeworks' => $homeworks
        ]);
    }
}
