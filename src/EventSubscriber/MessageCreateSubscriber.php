<?php

namespace App\EventSubscriber;

use App\Entity\Message;
use App\Entity\Thread;
use App\Event\MessageCreateEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class MessageCreateSubscriber implements EventSubscriberInterface
{
    /**
     * @param MessageCreateEvent $event
     */
    public function onCommunicationCreate(MessageCreateEvent $event)
    {
        if (
            !$event->getForm() instanceof FormInterface
            && !$event->getUser() instanceof UserInterface
            && !$event->getMessage() instanceof Message
        ) {
            return;
        }
        //todo:: still wtf? reik tvarkyt message forma.
        $message = $event->getMessage();
        $form = $event->getForm();

        $thread = new Thread();
        $thread
            ->setSubject($form->get('thread')->getData())
            ->addParticipant($event->getUser())
            ->addParticipant($form->get('participants')->getData());
        $message
            ->setRead(false)
            ->setCreatedAt()
            ->setOwner($event->getUser())
            ->setThread($thread);

        return $message;
    }

    public static function getSubscribedEvents()
    {
        return [
            'communication.create' => [
                ['onCommunicationCreate', 0]
            ]
        ];
    }
}
