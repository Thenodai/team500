<?php

namespace App\EventSubscriber;

use App\Entity\Message;
use App\Entity\Thread;
use App\Event\MessageSeenEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class MessageSeenSubscriber implements EventSubscriberInterface
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param MessageSeenEvent $entity
     */
    public function onCommunicationShow(MessageSeenEvent $entity)
    {

        if (!$entity->getThread() instanceof Thread && !$entity->getUser() instanceof UserInterface) {
            return;
        }

        $this->entityManager->getRepository(Message::class)->updateOldThreadMessagesAsSeen($entity->getThread(), $entity->getUser());

    }

    public static function getSubscribedEvents()
    {
        return [
            'communication.shown' => [
                ['onCommunicationShow', 0]
            ]
        ];
    }
}
