<?php

namespace App\EventSubscriber;

use App\Entity\Message;
use App\Entity\Thread;
use App\Event\MessageReplyEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class MessageReplySubscriber implements EventSubscriberInterface
{
    /**
     * @param MessageReplyEvent $entity
     */
    public function onCommunicationReply(MessageReplyEvent $entity)
    {
        if (!$entity->getThread() instanceof Thread
            && !$entity->getUser() instanceof UserInterface
            && !$entity->getMessage() instanceof Message
        ) {
            return;
        }

        //todo: wtf. reik taisyt mappinga formoj.
        return $entity->getMessage()
            ->setThread($entity->getThread())
            ->setCreatedAt()
            ->setOwner($entity->getUser())
            ->setRead(false);
    }

    public static function getSubscribedEvents()
    {
        return [
            'communication.reply' => [
                ['onCommunicationReply', 0]
            ]
        ];
    }
}
