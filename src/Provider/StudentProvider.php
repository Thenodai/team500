<?php

namespace App\Provider;

use App\Entity\Course;
use App\Entity\Homework;
use App\Entity\Lecture;
use App\Entity\SolvedHomework;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

class StudentProvider
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getLatestHomeworksByStudent(User $user)
    {
        return $this->entityManager->getRepository(Homework::class)->findVisibleHomeworksByUser($user, 5);
    }


    public function getCompletedHomeworksByStudent(User $user)
    {
        return $this->entityManager->getRepository(SolvedHomework::class)->findSolvedHomeworksWithMarks($user, 5);
    }

    public function getVisibleHomeworksByLecture(Lecture $lecture)
    {
        return $this->entityManager->getRepository(Homework::class)->findVisibleHomeworksByLecture($lecture);
    }

    public function getCoursesByStudent(User $user)
    {
        return $this->entityManager->getRepository(Course::class)->findCoursesByStudent($user);
    }

    public function getUnsolvedHomeworkByStudent(User $user)
    {
        return $this->entityManager->getRepository(Homework::class)->findVisibleHomeworksByUser($user);
    }
}
