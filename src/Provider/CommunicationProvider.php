<?php

namespace App\Provider;

use App\Entity\Message;
use App\Entity\Thread;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

class CommunicationProvider
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param User $user
     * @return Thread[]
     */
    public function getThreadsByUser(User $user)
    {
        return $this->entityManager->getRepository(Thread::class)->findThreadsByUser($user);
    }

    public function setOldMessagesAsSeen(Thread $thread, User $user)
    {
        return $this->entityManager->getRepository(Message::class)->updateOldThreadMessagesAsSeen($thread, $user);
    }

    public function getUnreadMessagesByUser(User $user)
    {
        return $this->entityManager->getRepository(Message::class)->findUnreadMessagesByUser($user);
    }
}
