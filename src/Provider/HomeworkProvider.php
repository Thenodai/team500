<?php

namespace App\Provider;

use App\Entity\Homework;
use App\Entity\SolvedHomework;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

class HomeworkProvider
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getHomeworksByTeacher(User $user)
    {
        return $this->entityManager->getRepository(Homework::class)->findAllHomeworkByTeacher($user);
    }

    public function getUnmarkedHomework(User $user)
    {
        return $this->entityManager->getRepository(SolvedHomework::class)->findUnmarkedHomework($user);
    }
}
