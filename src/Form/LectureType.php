<?php

namespace App\Form;

use App\Entity\Course;
use App\Entity\Lecture;
use App\Entity\User;
use App\Repository\CourseRepository;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LectureType extends AbstractType
{
    private $user;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->user = $options['user'];
        $builder
            ->add('title', TextType::class, [
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Enter title'
                ]
            ])
            ->add('description', TextareaType::class, [
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Enter title'
                ]
            ])
            ->add('course', EntityType::class, [
                'class' => Course::class,
                'query_builder' => function (CourseRepository $courseRepository) use ($options) {
                    return $courseRepository->createQueryBuilder('c')
                        ->where('c.user = :user')
                        ->setParameter('user', $this->user);
                },
                'choice_label' => 'title',
                'attr' => [
                    'class' => 'form-control mb-4'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Lecture::class,
            'user' => User::class
        ]);
    }
}
