<?php
/**
 * Created by PhpStorm.
 * User: Andrius
 * Date: 2018-05-21
 * Time: 19:19
 */

namespace App\Form;


use App\Entity\Course;
use App\Entity\User;
use App\Entity\UserCourse;
use App\Repository\UserRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserCourseEmbeddedForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user', EntityType::class,[
                'class' => User::class,
                'choice_label' => 'email',
                'query_builder' => function (UserRepository $repository){
                    return $repository->findAllStudents();
                }
            ])
            ->addEventListener(
                FormEvents::POST_SET_DATA,[$this, 'onPostSetData']
            );
    }

    public function onPostSetData(FormEvent $event){
        if($event->getData() && $event->getData()->getId()){
            $form = $event->getForm();
//            unset($form['user']);
//            unset($form['course']);
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
           'data_class' => UserCourse::class
        ]);
    }
}
