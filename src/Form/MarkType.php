<?php

namespace App\Form;

use App\Entity\Course;
use App\Entity\SolvedHomework;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MarkType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('mark', ChoiceType::class, array(
                'choices' => [
                    "10" => "10",
                    "9" => "9",
                    "8" => "8",
                    "7" => "7",
                    "6" => "6",
                    "5" => "5",
                    "4" => "4",
                    "3" => "3",
                    "2" => "2",
                    "1" => "1"
                ],
                'attr' => ['class' => 'form-control']
            ))
            ->add('Submit', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-primary mt-4'
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SolvedHomework::class,
        ]);
    }
}
