<?php

namespace App\Form;

use App\Entity\Message;
use App\Entity\Thread;
use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NewThreadMessageFormType extends AbstractType
{
    private $currentUser;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->currentUser = $options['user'];
        $builder
            ->add('participants', EntityType::class, [
                'label' => 'Send To',
                'class' => User::class,
                'query_builder' => function (UserRepository $userRepository) {
                    return $userRepository->findAllUsersExceptLoggedIn($this->currentUser);
                },
                'choice_label' => 'email',
                'mapped' => false,
            ])
            ->add('thread', TextType::class, [
                'label' => 'Subject',
                'data_class' => Thread::class,
                'mapped' => false,
                'property_path' => 'subject',
            ])
            ->add('body', TextareaType::class, [
                'label' => 'Message',
            ])
            ->add('Submit', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Message::class,
            'user' => User::class
        ]);
    }
}
