<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $roles = array();

        if($options['role'] == "ROLE_ADMIN")
        {
            $roles = array('ROLE_TEACHER' => 'ROLE_TEACHER', 'ROLE_STUDENT' => 'ROLE_STUDENT');
        }
        else
        {
            $roles = array('ROLE_STUDENT' => 'ROLE_STUDENT');
        }

        $builder
            ->add('name', TextType::class, ['attr' => ['class' => 'form-control', 'placeholder' => 'Enter name']])
            ->add('email', EmailType::class, ['attr' => ['class' => 'form-control', 'placeholder' => 'Enter email']])
            ->add('password', PasswordType::class, ['attr' => ['class' => 'form-control', 'placeholder' => 'Enter password']])
            ->add('role', ChoiceType::class, array(
                'choices' => $roles,
                'attr' => ['class' => 'form-control']
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'role' => ''
        ]);
    }
}
