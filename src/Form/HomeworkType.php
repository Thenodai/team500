<?php

namespace App\Form;

use App\Entity\Homework;
use App\Entity\Lecture;
use App\Entity\User;
use App\Repository\LectureRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\DataTransformer\DateTimeToStringTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class HomeworkType extends AbstractType
{
    /**
     * @var User
     */
    private $user;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->user = $options['user'];
        $builder
            ->add('title', TextType::class)
            ->add('description', TextareaType::class)
            ->add('type', ChoiceType::class, [
                'choices' => [
                    'text' => 'text',
                    'link' => 'link',
                    'file' => 'file',
                ]
            ])
            ->add('available_from', TextType::class, [
                'attr' => [
                    'class' => 'datepicker',
                    'data-date-format' => 'yyyy-mm-dd H:i:s',
                    'value' => '2018-05-15 21:05',
                ]
            ])
            ->add('lecture', EntityType::class, [
                'class' => Lecture::class,
                'choice_label' => 'title',
                'query_builder' => function (LectureRepository $lectureRepository) {
                    return $lectureRepository->findLecturesByTeacher($this->user);
                },
                'multiple' => false,
                'expanded' => false,
            ]);

        $builder->get('available_from')->addModelTransformer(new DateTimeToStringTransformer());
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Homework::class,
            'user' => User::class,
        ]);
    }
}
