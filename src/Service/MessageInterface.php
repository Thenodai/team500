<?php

namespace App\Service;

use Symfony\Component\Security\Core\User\UserInterface;

interface MessageInterface
{
    /**
     * @return integer
     */
    public function getId();

    /**
     * @param string $body
     * @return $this
     */
    public function setBody(string $body);

    /**
     * @return string
     */
    public function getBody();

    /**
     * @return \DateTimeInterface
     */
    public function setCreatedAt();

    /**
     * @return \DateTimeInterface
     */
    public function getCreatedAt();

    /**
     * @param ThreadInterface $thread
     */
    public function setThread(ThreadInterface $thread);

    /**
     * @return ThreadInterface
     */
    public function getThread();

    /**
     * @return UserInterface
     */
    public function getOwner();

    /**
     * @param UserInterface $user
     */
    public function setOwner(UserInterface $user);

    public function isRead();

    /**
     * @param bool $read
     */
    public function setRead(bool $read);
}