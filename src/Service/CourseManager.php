<?php

namespace App\Service;

use App\Entity\Course;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

class CourseManager
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getCoursesByUser(User $user)
    {
        /** CourseRepository $repository */
        $repository = $this->entityManager->getRepository(Course::class);
        if (in_array('ROLE_TEACHER', $user->getRoles())) {
            return $repository->findBy(['user' => $user]);
        }

        return $repository->findAll();
    }
}
