<?php

namespace App\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\User\UserInterface;

interface ThreadInterface
{
    /**
     * @return integer
     */
    public function getId();

    /**
     * @param string $subject
     * @return $this
     */
    public function setSubject(string $subject);

    /**
     * @return string
     */
    public function getSubject();

    /**
     * @param MessageInterface $message
     */
    public function setMessages(MessageInterface $message);

    /**
     * @return MessageInterface[]
     */
    public function getMessages();

    /**
     * @param UserInterface $user
     */
    public function addParticipant(UserInterface $user);

    /**
     * @return UserInterface[]|ArrayCollection
     */
    public function getParticipants();

    /**
     * @param UserInterface $user
     */
    public function removeParticipant(UserInterface $user);
}
