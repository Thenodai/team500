<?php

namespace App\Extension;

use Doctrine\ORM\PersistentCollection;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class MessageInversionExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('reverse', [$this, 'reverse'], ['is_safe' => ['html']])
        ];
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('reverse', [$this, 'reverse'])
        ];
    }

    /**
     * @param PersistentCollection $messages
     * @return array|PersistentCollection
     */
    public function reverse($messages)
    {
        if ($messages instanceof PersistentCollection) {
            return array_reverse($messages->toArray());
        }

        return $messages;
    }
}
