<?php

namespace App\Security\Voter;

use App\Entity\Course;
use App\Entity\Homework;
use App\Entity\Lecture;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class StudentVoter extends Voter
{
    const COURSE_SHOW = 'course_show';
    const LECTURE_SHOW = 'lecture_show';
    const HOMEWORK_SHOW = 'homework_show';
    const ROLE_ADMIN = 'ROLE_ADMIN';

    /**
     * @var AccessDecisionManagerInterface
     */
    private $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, [self::COURSE_SHOW, self::LECTURE_SHOW, self::HOMEWORK_SHOW])) {
            return false;
        }
        if (!$subject instanceof Course && !$subject instanceof Lecture && !$subject instanceof Homework) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        /** @var User $user */
        $user = $token->getUser();
        if (!$user instanceof UserInterface) {
            return false;
        }

        if ($this->decisionManager->decide($token, [self::ROLE_ADMIN])) {
            return true;
        }

        $userCourses = $user->getUserCourse()->getIterator();

        switch ($attribute) {
            case self::COURSE_SHOW:
                return $this->canViewAssignedCourse($subject, $userCourses);
                break;
            case self::LECTURE_SHOW:
                return $this->canViewAssignedLecture($subject, $userCourses);
                break;
            case self::HOMEWORK_SHOW:
                return $this->canViewAssignedHomework($subject, $userCourses);
                break;
        }

        return false;
    }

    private function canViewAssignedCourse(Course $course, \ArrayIterator $userCourses)
    {
        foreach ($userCourses as $userCourse) {
            if ($course->getUserCourse()->contains($userCourse)) {
                return true;
            }
        }

        return false;
    }

    private function canViewAssignedLecture(Lecture $lecture, \ArrayIterator $userCourses)
    {
        foreach ($userCourses as $userCourse) {
            if ($lecture->getCourse()->getUserCourse()->contains($userCourse)) {
                return true;
            }
        }

        return false;
    }

    private function canViewAssignedHomework(Homework $homework, \ArrayIterator $userCourses)
    {
        foreach ($userCourses as $userCourse) {
            if ($homework->getLecture()->getCourse()->getUserCourse()->contains($userCourse)) {
                return true;
            }
        }

        return false;
    }
}
