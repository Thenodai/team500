<?php

namespace App\Security\Voter;

use App\Entity\Lecture;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class LectureVoter extends Voter
{
    const CREATE = 'create';
    const EDIT = 'edit';
    const SHOW = 'show';
    const DELETE = 'delete';
    const ROLE_ADMIN = 'ROLE_ADMIN';
    const ROLE_TEACHER = 'ROLE_TEACHER';

    /**
     * @var AccessDecisionManagerInterface
     */
    private $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, [self::CREATE, self::EDIT, self::SHOW, self::DELETE])) {
            return false;
        }
        if (!$subject instanceof Lecture) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        /** @var User $user */
        $user = $token->getUser();
        if (!$user instanceof UserInterface) {
            return false;
        }

        if ($this->decisionManager->decide($token, [self::ROLE_ADMIN])) {
            return true;
        }

        /** @var Lecture $lecture */
        $lecture = $subject;

        switch ($attribute) {
            case self::SHOW:
                return $this->ownerCanView($token, $lecture, $user);
                break;
            case self::CREATE:
                return $this->canCreate($token);
                break;
            case self::EDIT:
                return $this->ownerCanEdit($token, $lecture, $user);
                break;
            case self::DELETE:
                return $this->ownerCanDelete($token, $lecture, $user);
                break;
        }

        return false;
    }

    private function ownerCanEdit($token, Lecture $lecture, User $user)
    {
        if ($this->decisionManager->decide($token, [self::ROLE_TEACHER])) {
            if ($user === $lecture->getUser()) {
                return true;
            }
        }

        return false;
    }

    private function canCreate($token)
    {
        if ($this->decisionManager->decide($token, [self::ROLE_TEACHER])) {
            return true;
        }

        return false;
    }

    private function ownerCanView($token, Lecture $lecture, User $user)
    {
        if ($this->decisionManager->decide($token, [self::ROLE_TEACHER])) {
            if ($user === $lecture->getUser()) {
                return true;
            }
        }

        return false;
    }

    private function ownerCanDelete($token, Lecture $lecture, User $user)
    {
        if ($this->decisionManager->decide($token, [self::ROLE_TEACHER])) {
            if ($user === $lecture->getUser()) {
                return true;
            }
        }

        return false;
    }
}
