<?php

namespace App\Security\Voter;

use App\Entity\Course;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class CourseVoter extends Voter
{
    const CREATE = 'create';
    const EDIT = 'edit';
    const SHOW = 'show';
    const DELETE = 'delete';
    const ROLE_ADMIN = 'ROLE_ADMIN';
    const ROLE_TEACHER = 'ROLE_TEACHER';

    /**
     * @var AccessDecisionManagerInterface
     */
    private $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, [self::CREATE, self::EDIT, self::SHOW, self::DELETE])) {
            return false;
        }
        if (!$subject instanceof Course) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        if (!$user instanceof UserInterface) {
            return false;
        }

        if ($this->decisionManager->decide($token, [self::ROLE_ADMIN])) {
            return true;
        }

        $course = $subject;

        switch ($attribute) {
            case self::SHOW:
                return $this->ownerCanView($token, $course, $user);
                break;
            case self::CREATE:
                return $this->canCreate($token);
                break;
            case self::EDIT:
                return $this->ownerCanEdit($token, $course, $user);
                break;
            case self::DELETE:
                return $this->ownerCanDelete($token, $course, $user);
                break;
        }

        return false;
    }

    private function ownerCanEdit($token, Course $course, User $user)
    {
        if ($this->decisionManager->decide($token, [self::ROLE_TEACHER])) {
            if ($user === $course->getUser()) {
                return true;
            }
        }

        return false;
    }

    private function canCreate($token)
    {
        if ($this->decisionManager->decide($token, [self::ROLE_TEACHER])) {
            return true;
        }

        return false;
    }

    private function ownerCanView($token, Course $course, User $user)
    {
        if ($this->decisionManager->decide($token, [self::ROLE_TEACHER])) {
            if ($user === $course->getUser()) {
                return true;
            }
        }

        return false;
    }

    private function ownerCanDelete($token, Course $course, User $user)
    {
        if ($this->decisionManager->decide($token, [self::ROLE_TEACHER])) {
            if ($user === $course->getUser()) {
                return true;
            }
        }

        return false;
    }
}
