<?php

namespace App\Security\Voter;

use App\Entity\Message;
use App\Entity\Thread;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class CommunicationVoter extends Voter
{
    const CREATE_COMMUNICATION = 'create_communication';
    const SHOW_COMMUNICATION = 'show_communication';
    const DELETE_THREAD = 'delete_thread';
    const DELETE_MESSAGE = 'delete_message';
    const ROLE_ADMIN = 'ROLE_ADMIN';
    const ROLE_STUDENT = 'ROLE_STUDENT';

    /**
     * @var AccessDecisionManagerInterface
     */
    private $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, [self::CREATE_COMMUNICATION, self::SHOW_COMMUNICATION, self::DELETE_THREAD, self::DELETE_MESSAGE])) {
            return false;
        }
        if (!$subject instanceof Thread && !$subject instanceof Message) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        /** @var User $user */
        $user = $token->getUser();
        if (!$user instanceof UserInterface) {
            return false;
        }

        if ($this->decisionManager->decide($token, [self::ROLE_ADMIN])) {
            return true;
        }

        switch ($attribute) {
            case self::CREATE_COMMUNICATION:
                return $this->canCreate($token);
                break;
            case self::SHOW_COMMUNICATION:
                return $this->canSee($subject, $user);
                break;
            case self::DELETE_THREAD:
                return $this->canDeleteThread($subject, $user);
                break;
            case self::DELETE_MESSAGE:
                return $this->canDeleteMessage($subject, $user);
                break;
        }

        return false;
    }

    private function canCreate($token)
    {
        if ($this->decisionManager->decide($token, [self::ROLE_STUDENT])) {
            return true;
        }

        return false;
    }

    private function canSee(Thread $thread, User $user)
    {
        if ($thread->getParticipants()->contains($user)) {
            return true;
        }

        return false;
    }

    private function canDeleteThread(Thread $thread, User $user)
    {
        if ($thread->getParticipants()->contains($user)) {
            return true;
        }

        return false;
    }

    private function canDeleteMessage(Message $message, User $user)
    {
        if ($message->getOwner() === $user) {
            return true;
        }

        return false;
    }
}
