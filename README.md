# README #


## How do I get set up? ###

- Clone this repository
- `docker-compose build`
- `docker-compose up -d`
- `docker-compose exec -u 1000 php-fpm composer install`
- If you are using Linux / Docker for Mac / Docker for windows - navigate to localhost:8080
- If you are using Docker Toolbox - you need to add an entry to your /etc/hosts file with an IP to your VM. It can be found by running `docker-machine config`. Afterwards, navigate to <your-docker-host.local>:8080

## Webpack commands:

#### Compile assets once:
./node_modules/.bin/encore dev

#### Compile assets on file change:
./node_modules/.bin/encore dev --watch

#### Compile assets and minify them:
./node_modules/.bin/encore production

#### Same commands with yarn:

- yarn run encore dev
- yarn run encore dev --watch
- yarn run encore production


## Webpack builds:

- webpack takes css from assets/css/app.scss (any aditional css files should be included to this file) and builds it to public/build/cdd/app.scc
- webpack takes js from assets/js/app.js and builds it to public/build/js/app.js